#!/usr/bin/env python
import os
import sys
import argparse
from unittest import skip

try:
    import pandas
    import xlsxwriter
except ImportError:
    pandas = None


def main():
    p = argparse.ArgumentParser()
    p.add_argument(
        "-s",
        "--skip",
        action="append",
        default=[],
        help="Do not include in combined stats",
    )
    p.add_argument(
        "-x",
        action="append",
        default=["Baty,Porter", "S.,Porter", "Holbrook,Dallas"],
        help="Do not include in optimized batting order",
    )
    p.add_argument(
        "--fmt", metavar="output format", action="append", choices=("csv", "xlsx")
    )
    p.add_argument("-o", help="Output file basename", default="DINGERS")
    p.add_argument("files", nargs="+")
    args = p.parse_args()
    if not args.fmt:
        args.fmt = ["xlsx"] if pandas is not None else ["csv"]
    if not pandas:
        if "xlsx" in args.fmt:
            print("The pandas and xlsxwriter modules are required to write xlsx")
            args.fmt = ["csv"]
    skip_players = []
    for arg in args.skip:
        last, first = arg.split(",")
        skip_players.append((last, first))
    skip_batting = []
    for arg in args.x:
        last, first = arg.split(",")
        skip_batting.append((last, first))
    odata = combine_offense(
        *args.files, skip_players=skip_players, skip_batting=skip_batting
    )

    if "xlsx" in args.fmt:
        write_xlsx(args.o, Offense=odata)
    if "csv" in args.fmt:
        dump_csv(odata, "OFFENSE.csv")


def compute_batting_order(table, skip_players=None):
    """Compute the batting order from GC data

    Parameters
    ----------
    table : list of list
        table[0] is the header
        table[i] is the data for the ith player

    Notes
    -----
    The batting order is computed as:

      1 Best OBP of all players
      4 Best SLG of remaining players
      2 Best OPS of remaining players
      3 Next best OPS of remaining players
      5-11: Sort remaining players by OBP, SLG, -SO% (strike out percentage)

    See https://www.sportsbettingdime.com/guides/strategy/batting-order-sabermetrics

    """
    skip_players = skip_players or []
    order = [None] * 4

    map = dict([(key, col) for (col, key) in enumerate(table[0])])
    data = []
    for row in table[1:]:
        name = row[map["Name"]].strip()
        if name == "Total":
            continue
        first, last = name.split()
        if (last, first) in skip_players:
            continue
        data.append(row)

    def argmax(data, col, skip=lambda x: False):
        maxrow = 0
        if skip(maxrow):
            maxrow = 1
        for (row, values) in enumerate(data):
            if skip(row):
                continue
            if values[col] > data[maxrow][col]:
                maxrow = row
        return maxrow

    order[0] = argmax(data, map["OBP"], skip=lambda x: True if x in order else False)
    order[3] = argmax(data, map["SLG"], skip=lambda x: True if x in order else False)
    order[1] = argmax(data, map["OPS"], skip=lambda x: True if x in order else False)
    order[2] = argmax(data, map["OPS"], skip=lambda x: True if x in order else False)

    def sort_key(i):
        x = data[i]
        metric = round(x[map["OBP"]], 1), round(x[map["SLG"]], 1) - x[map["SO%"]]
        return metric

    remaining = sorted([i for i in range(len(data)) if i not in order], key=sort_key)
    order.extend(reversed(remaining))

    table = [["Order", "Number", "Last", "First"]]
    for (i, rowno) in enumerate(order, start=1):
        row = data[rowno]
        name = row[map["Name"]].strip()
        first, last = name.split()
        number = row[map["Number"]]
        table.append([i, number, last, first])

    return table


def combine_offense(*files, skip_players=None, skip_batting=None):
    def blacklisted(stat):
        # The fields below can be computed from raw data
        if stat in ("AB", "AVG", "OBP", "OPS", "SLG", "BABIP", "XBH"):
            return True
        elif "%" in stat:
            return True
        elif "/" in stat:
            return True
        return False

    skip_players = skip_players or []
    data = {}
    total = {"Number": "", "Order": "", "GP": ""}
    for file in files:
        content = read_gc_file(file)
        prologue = content[0]
        start = prologue.index("Batting")
        end = prologue.index("Pitching")
        header = content[1][start:end]
        map = dict([(x, i) for (i, x) in enumerate(header)])
        for parts in content[2:]:
            no, last, first = parts[:3]
            parts = parts[start:end]
            id = (last, first)
            if id in skip_players:
                continue
            pd = data.setdefault(id, {})
            pd["Number"] = "" if not no else int(no)
            pd["Order"] = None
            for key in header:
                if blacklisted(key):
                    continue
                current = integer(parts[map[key]])
                pd[key] = current + pd.get(key, 0)

    total = {}
    for (id, x) in data.items():
        for (key, value) in x.items():
            if key in ("Number", "Order", "GP"):
                total[key] = ""
            else:
                current = total.get(key, 0)
                total[key] = value + total.get(key, 0)
    data[("Total", "")] = total

    for (id, x) in data.items():
        x["AB"] = x["H"] + x["BB"] + x["SO"] + x["HBP"] + x["SAC"] + x["ROE"] + x["FC"]
        x["AVG"] = x["H"] / x["AB"]
        x["AVG+"] = (x["H"] + x["ROE"] + x["SAC"]) / x["AB"]
        x["XBH"] = x["2B"] + x["3B"] + x["HR"]
        x["XBH/AB"] = x["XBH"] / x["AB"]
        x["OBP"] = (x["H"] + x["BB"] + x["HBP"]) / (
            x["AB"] + x["BB"] + x["HBP"] + x["SAC"]
        )
        x["OBP+"] = (x["H"] + x["BB"] + x["HBP"] + x["ROE"]) / (
            x["AB"] + x["BB"] + x["HBP"] + x["SAC"] + x["ROE"]
        )
        x["SLG"] = (x["1B"] + 2 * x["2B"] + 3 * x["3B"] + 4 * x["HR"]) / x["AB"]
        x["OPS"] = x["OBP"] + x["SLG"]
        x["OPS+"] = x["OBP+"] + x["SLG"]
        x["C%"] = (x["AB"] - x["SO"]) / x["AB"]
        x["SO%"] = x["SO"] / x["AB"]
        x["KL%"] = x["K-L"] / x["AB"]
        x["PA/BB"] = "" if not x["BB"] else x["PA"] / x["BB"]
        x["BB/K"] = "" if not x["SO"] else x["BB"] / x["SO"]
        x["AB/HR"] = "" if not x["HR"] else x["AB"] / x["HR"]
        x["PS/PA"] = x["PS"] / x["PA"]
        x["QAB%"] = x["QAB"] / x["PA"]
        x["SO/PA"] = x["SO"] / x["PA"]

    table = []
    header = ["Name"] + list(list(data.values())[0].keys())
    table.append(header)
    for (id, x) in data.items():
        last, first = id
        values = [x.get(key) for key in header[1:]]
        row = [f"{first} {last}"] + ["" if _ is None else _ for _ in values]
        table.append(row)

    order = compute_batting_order(table, skip_players=skip_batting)
    j = header.index("Order")
    for row in order[1:]:
        i, no, last, first = row
        name = " ".join((first, last))
        for trow in table:
            if name == trow[0]:
                trow[j] = i
                break

    return table


def dump_csv(data, filename):
    with open(filename, "w") as fh:
        for row in data:
            fh.write(join(row, sep=",") + "\n")
    return


def strip_quotes(string):
    quote_chars = ("'", '"')
    for quote_char in quote_chars:
        if string.startswith(quote_char) and string.endswith(quote_char):
            return strip_quotes(string[1:-1])
    return string


def read_gc_file(file):
    content = []
    with open(file) as fh:
        for line in fh:
            parts = split(line, sep=",", key=strip_quotes, skip_empty=False)
            if parts[0] == "Totals":
                break
            content.append(parts)
    return content


def write_xlsx(basename, **kwargs):
    combined_filename = f"{basename}.xlsx"
    writer = pandas.ExcelWriter(combined_filename, engine="xlsxwriter")

    dfs = []
    for (sheet_name, data) in kwargs.items():
        columns = list(data[0])
        df = pandas.DataFrame(data[1:], columns=columns)
        df.to_excel(
            writer, sheet_name=sheet_name, startrow=1, header=False, index=False
        )
        dfs.append((sheet_name, df))

    workbook = writer.book
    fmt = {"bold": True, "text_wrap": True, "border": 0}
    for (sheet_name, df) in dfs:
        header = list(df.columns)
        worksheet = writer.sheets[sheet_name]
        header_format = workbook.add_format(fmt)
        for colnum, value in enumerate(header):
            worksheet.write(0, colnum, value, header_format)
        for column in df:
            width = max(max([len(stringify(x)) for x in df[column]]), len(column))
            col_idx = df.columns.get_loc(column)
            worksheet.set_column(col_idx, col_idx, width + 2)
        worksheet.freeze_panes(1, 1)

    writer.save()


def split(string, sep=None, maxsplit=-1, key=lambda x: x, skip_empty=True):
    parts = [key(x.strip()) for x in string.split(sep, maxsplit=maxsplit)]
    if skip_empty:
        parts = [x for x in parts if x.split()]
    return parts


def stringify(item):
    if isinstance(item, float):
        return "{0:.3f}".format(item)
    elif isinstance(item, int):
        return "{0:d}".format(item)
    else:
        return str(item)


def join(sequence, sep=" "):
    return sep.join([stringify(x) for x in sequence])


def integer(string):
    if string == "-":
        return 0
    return int(float(string))


if __name__ == "__main__":
    sys.exit(main())
