# combine_gc_data

Combine GameChanger data from multiple teams

## Usage

```console
combine_gc_data.py file1.csv [file2.csv [... [filen.csv]]]
```

where `file*.csv` are files exported by GameChanger.
